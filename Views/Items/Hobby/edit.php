<?php

include_once('../../../vendor/autoload.php');

use App\Items\Hobby\Hobby;
use App\Items\Utility\Utility;

$hobby= new Hobby();

$singleRow = $hobby->prepare($_GET)->view();

$singleHobbyList = $singleRow['hobbies'];
$singleHobbyArray = explode(",", $singleHobbyList);

//Utility::dd($singleHobbyArray);

?>



<!DOCTYPE html>
<html lang="en">
<head>
  <title>Hobby</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Update Your Hobby</h2>
  
  <form method="post" action="update.php">

    <div class="form-group">
        <input type="hidden" name="id" value="<?php echo $singleRow['id'] ; ?>">
        <label>Name:</label>
         <input type="text" class="form-control" id="usr" name="name" value="<?php echo $singleRow['name'] ; ?>">
    </div>
    <div class="form-group">
        <label>Last Name:</label>
         <input type="text" class="form-control" id="usr" name="lastname" value="<?php echo $singleRow['lastname'] ; ?>">
    </div>

    <div class="checkbox">
      <label><input type="checkbox" name="hobby[]" value="Coding" <?php if(in_array("Coding", $singleHobbyArray)){echo "Checked";} else{echo "";}?>>Coding</label>
    </div>
    <div class="checkbox">
      <label><input type="checkbox" name="hobby[]" value="Cycling" <?php if(in_array("Cycling", $singleHobbyArray)){echo "Checked";} else{echo "";}?>>Cycling</label>
    </div>
    <div class="checkbox">
      <label><input type="checkbox" name="hobby[]" value="Swimming" <?php if(in_array("Swimming", $singleHobbyArray)){echo "Checked";} else{echo "";}?>>Swimming</label>
    </div>
    <div class="checkbox">
      <label><input type="checkbox" name="hobby[]" value="Playing Football" <?php if(in_array("Playing Football", $singleHobbyArray)){echo "Checked";} else{echo "";}?>>Playing Football</label>
       <div class="checkbox">
      <label><input type="checkbox" name="hobby[]" value="Watching Movie" <?php if(in_array("Watching Movie", $singleHobbyArray)){echo "Checked";} else{echo "";}?>>Watching Movie</label>
    </div>
    </div>
    <input type="submit" value="submit">
  </form>
</div>

</body>
</html>

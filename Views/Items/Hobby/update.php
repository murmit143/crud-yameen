<?php

include_once('../../../vendor/autoload.php');

use App\Items\Hobby\Hobby;
use App\Items\Utility\Utility;

//Utility::d($_POST);

$array_hobby = $_POST['hobby'];

//Utility::d($array_hobby);

$comma_separated_string = implode(",", $array_hobby);
//Utility::d($comma_separated_string);

$_POST['hobby'] = $comma_separated_string;

//Utility::d($_POST);

$hobby = new Hobby();

$hobby->prepare($_POST)->update();

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Hobby</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Select Your Hobby</h2>
  
  <form method="post" action="store.php">

    <div class="form-group">
        <label>Name:</label>
         <input type="text" class="form-control" id="usr" name="name">
    </div>
    <div class="form-group">
        <label>Last Name:</label>
         <input type="text" class="form-control" id="usr" name="lastname">
    </div>

    <div class="checkbox">
      <label><input type="checkbox" name="hobby[]" value="Coding">Coding</label>
    </div>
    <div class="checkbox">
      <label><input type="checkbox" name="hobby[]" value="Cycling">Cycling</label>
    </div>
    <div class="checkbox">
      <label><input type="checkbox" name="hobby[]" value="Swimming">Swimming</label>
    </div>
    <div class="checkbox">
      <label><input type="checkbox" name="hobby[]" value="Playing Football">Playing Football</label>
       <div class="checkbox">
      <label><input type="checkbox" name="hobby[]" value="Watching Movie">Watching Movie</label>
    </div>
    </div>
    <input type="submit" value="submit">
  </form>
</div>

</body>
</html>

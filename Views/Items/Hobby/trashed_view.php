<?php

session_start();
include_once('../../../vendor/autoload.php');
use App\Items\Hobby\Hobby;
use App\Items\Utility\Utility;
use App\Items\Message\Message;


$hobby = new Hobby();
$trashedHobby = $hobby->trashed();

//Utility::dd($trashedHobby);
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>All Hobby</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>All Hobby List</h2>
   
    <a href="index.php" class="btn btn-info" role="button">All Hobby List</a>
    </br></br>
    <form action="recoverMultiple.php" method="post" id="multiple"> 
    <button type="submit" class="btn btn-info">Recover Selected</button>	
    <button type="button" class="btn btn-primary" id="delete">Delete All  Selected</button>

    <div id="message">
      <?php 
        if(array_key_exists('message', $_SESSION) && !empty($_SESSION['message']))
        {

          echo Message::message(); 
        }

      ?>
    </div>
            
  <table class="table">
    <thead>
      <tr>
      	<th>Select</th>
        <th>Sl No.</th>
        <th>ID</th>
        <th>Name</th>
        <th>Last Name</th>
        <th>Hobby</th>
        <th>Action</th>
      </tr>
     

    </thead>
    <tbody>
    	<?php 
    	$sl= 0;
    	foreach($trashedHobby as $trash){	
    	$sl++	
    	?>
      <tr>
      	<td><input type="checkbox" name=mark[] value=<?php echo $trash['id']; ?>></td>
      	<td><?php echo $sl; ?></td>
        <td><?php echo $trash['id']; ?></td>
        <td><?php echo $trash['name']; ?></td>
        <td><?php echo $trash['lastname']; ?></td>
        <td><?php echo $trash['hobbies']; ?></td>
        <td>
        	<a href="recover.php?id=<?php echo $trash['id']; ?>" class="btn btn-primary" role="button">Recover</a>
        	<a href="delete.php?id=<?php echo $trash['id']; ?>" class="btn btn-danger" role="button">Delete</a>        
        </td>
      </tr>
       <?php }  ?>
     
    </tbody>
  </table>
  </form>  

</div>

<script>
  $('#delete').on('click',function(){
      document.forms[0].action="deleteMultiple.php";
     $('#multiple').submit();
 });


  $('#message').show().delay(3000).fadeOut();
</script>

</body>
</html>

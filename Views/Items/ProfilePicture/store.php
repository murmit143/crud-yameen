<?php

include_once('../../../vendor/autoload.php');


use App\Items\ProfilePicture\ImageUploader;
use App\Items\Utility\Utility;

//Utility::d($_FILES);

if((isset($_FILES['image'])) && !empty($_FILES['image']['name'])){

	$imageName = time().$_FILES['image']['name'];
	$temporary_location = $_FILES['image']['tmp_name'];

	move_uploaded_file($temporary_location, '../../../Resources/Images/'.$imageName);

	$_POST['image'] = $imageName;

	//Utility::d($_POST);

}

$profilePicture = new ImageUploader();

$profilePicture->prepare($_POST)->store();
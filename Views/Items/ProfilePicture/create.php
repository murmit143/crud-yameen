<!DOCTYPE html>
<html lang="en">
<head>
  <title>Profile Picture</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1"> 
  <link rel="stylesheet" type="text/css" href="../../../Resources/bootstrap/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="../../../Resources/bootstrap/js/bootstrap.js">
</head>
<body>

<div class="container">
  <h2>Create Profile</h2>
  <form action="store.php" method="post" enctype="multipart/form-data">
    <div class="form-group">
      <label>Name:</label>
      <input type="text" class="form-control" placeholder="Enter Your Name" name="name">
    </div>
    <div class="form-group">
      <label>Upload File:</label>
      <input type="file" class="form-control" name="image">
    </div>
    
    <input type="submit" class="btn btn-default" value="Submit">
  </form>
</div>

</body>
</html>

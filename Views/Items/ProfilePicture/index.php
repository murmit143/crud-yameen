<?php

session_start();

include_once('../../../vendor/autoload.php');
use App\Items\ProfilePicture\ImageUploader;
use App\Items\Utility\Utility;
use App\Items\Message\Message;


$profilePicture= new ImageUploader();
$allInfo= $profilePicture->index();

//Utility::dd($allBook);
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>All Book</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>User List</h2>
   
    <a href="create.php" class="btn btn-info" role="button">Add User Info</a>
    <a href="trashed_view.php" class="btn btn-primary" role="button">Trashed List</a>
  </br>
    

    <div id="message">
      <?php 
        if(array_key_exists('message', $_SESSION) && !empty($_SESSION['message'])){

      echo Message::message(); }

      ?>
    </div>
            
  <table class="table">
    <thead>
      <tr>
        <th>Sl No.</th>
        <th>ID</th>
        <th>Name</th>
        <th>Profile Picture</th>
      </tr>
     

    </thead>
    <tbody>
      <?php 
      $sl= 0;
      foreach($allInfo as $info){ 
      $sl++ 
        ?>
      <tr>
        <td><?php echo $sl; ?></td>
        <td><?php echo $info['id']; ?></td>
        <td><?php echo $info['name']; ?></td>
        <td><?php echo '<img src="../../../Resources/Images/'.$info['images']; ?></td>

        <td><a href="view.php?id=<?php echo $book['id']; ?>" class="btn btn-info" role="button">View</a>
          <a href="edit.php?id=<?php echo $book['id']; ?>" class="btn btn-primary" role="button">Edit</a>
          <a href="delete.php?id=<?php echo $book['id']; ?>" class="btn btn-danger" role="button">Delete</a>
          <a href="trash.php?id=<?php echo $book['id']; ?>" class="btn btn-danger" role="button">Trash</a>
        </td>
      </tr>
       <?php }  ?>
     
    </tbody>
  </table>
</div>

<script>
  $('#message').show().delay(3000).fadeOut();
</script>

</body>
</html>

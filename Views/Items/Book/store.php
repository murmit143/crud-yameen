<?php

include_once('../../../vendor/autoload.php');
use App\Items\Book\Book;

$book = new Book();
$book->prepare($_POST);
$book->store();
<?php

include_once('../../../vendor/autoload.php');

use App\Items\Book\Book;
use App\Items\Book\Utility;

$book = new Book();

$singleData = $book->prepare($_GET)->view();

//Utility::d($singleData);

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2><?php echo $singleData['title']; ?></h2>
  <ul class="list-group">
    <li class="list-group-item">Id: <?php echo $singleData['id']; ?></li>
    <li class="list-group-item">Book Title: <?php echo $singleData['title']; ?></li>
    
  </ul>
</div>

</body>
</html>

<?php

include_once('../../../vendor/autoload.php');

use App\Items\Book\Book;

$book = new Book();

$singleItem = $book->prepare($_GET)->view();


?>



<!DOCTYPE html>
<html lang="en">
<head>
  <title>CRUD</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1"> 
  <link rel="stylesheet" type="text/css" href="../../../Resources/bootstrap/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="../../../Resources/bootstrap/js/bootstrap.js">
</head>
<body>
 
<div class="container">
  <h2>Atomic Project</h2>
  <form action="update.php" method="post">
    <div class="form-group">
      <label>Update Book Title:</label>
      <input type="hidden"  name="id" value="<?php echo $singleItem['id'] ?>">
      <input type="text" class="form-control" name="title" value="<?php echo $singleItem['title'] ?>">
    </div>
    
    <button type="submit" class="btn btn-default">Update</button>
  </form>
</div>

</body>
</html>

<?php
session_start();


include_once('../../../vendor/autoload.php');
use App\Items\Book\Book;
use App\Items\Utility\Utility;
use App\Items\Message\Message;


$book = new Book();
$trahedBook = $book->trashed();

//Utility::dd($allBook);
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>All Book</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>All Book List</h2>
   
    
    <a href="index.php" class="btn btn-primary" role="button">All Book List</a>
  </br></br>
   <form action="recoverMultiple.php" method="post"  id="multiple">
   <button type="submit" class="btn btn-info">Recover Selected</button>
   <button type="button" class="btn btn-info" id="delete">Delete all Selected</button>
   </br></br>  

    <div id="message">

      <?php 
        if(array_key_exists('message', $_SESSION) && !empty($_SESSION['message'])){

      echo Message::message(); }

      ?>
    </div>

  <table class="table">
    <thead>
      <tr>
        <th>Select</th>
        <th>Sl No.</th>
        <th>ID</th>
        <th>Book Title</th>
        <th>Action</th>
      </tr>
     

    </thead>
    <tbody>
    	<?php 
    	$sl= 0;
    	foreach($trahedBook as $trash){	
    	
      $sl++	
    		?>
      <tr>
        <td><input type="checkbox" name=mark[] value=<?php echo $trash['id']; ?>></td>
      	<td><?php echo $sl; ?></td>
        <td><?php echo $trash['id']; ?></td>
        <td><?php echo $trash['title']; ?></td>
        <td>
        	<a href="recover.php?id=<?php echo $trash['id']; ?>" class="btn btn-primary" role="button">Recover</a>
        	<a href="delete.php?id=<?php echo $trash['id']; ?>" class="btn btn-danger" role="button">Delete</a>
        
        </td>
      </tr>
       <?php }  ?>
     
    </tbody>
  </table>
</form>
</div>

<script>
  $('#delete').on('click',function(){
      document.forms[0].action="deleteMultiple.php";
     $('#multiple').submit();
 });

  $('#message').show().delay(3000).fadeOut();
</script>

</body>
</html>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>CRUD</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1"> 
  <link rel="stylesheet" type="text/css" href="../../../Resources/bootstrap/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="../../../Resources/bootstrap/js/bootstrap.js">
</head>
<body>

<div class="container">
  <h2>Atomic Project</h2>
  <form action="store.php" method="post">
    <div class="form-group">
      <label>Enter Book Title:</label>
      <input type="text" class="form-control" id="email" placeholder="Enter book title" name="title">
    </div>
    
    <button type="submit" class="btn btn-default">Submit</button>
  </form>
</div>

</body>
</html>

<?php

namespace App\Items\Book;
use App\Items\Message\Message;
use App\Items\Utility\Utility;

class Book {

		public $id = "";
		public $title = "";
		public $conn;
		public $deleted_at;

		public function prepare($data=""){
			if(array_key_exists("title", $data)){
				$this->title = $data['title'];
			}

			if(array_key_exists("id", $data)){
				$this->id = $data['id'];
			}

			return $this;
		}

		public function __construct(){
			$this->conn = mysqli_connect("localhost","root","","atomicprojectcrud") or die("Database connection failed");
		}

		public function store(){

			$query = "INSERT INTO `book` (`title`) VALUES ('".$this->title."')";
			//echo $query;
			
			$result = mysqli_query($this->conn, $query);

			if($result){
				Message::message("<div class=\"alert alert-success\">
 				 <strong>Success!</strong> Data has been stored successfully.
					</div>");
				Utility::redirect('index.php');
			}
			else{
				Message::message("<div class=\"alert alert-danger\">
 				 <strong>Oops!</strong> Data has not been stored successfully.
					</div>");
				header('Location:index.php');

				}

			}

		public function index(){

			$_allBook = array();
			$query = "SELECT * FROM `book` WHERE `deleted_at` IS NULL";
			$result = mysqli_query($this->conn, $query);
			while($row = mysqli_fetch_assoc($result)){
				$_allBook[] = $row;
			}
			return $_allBook;

		}
			
		
		public function view(){
			$query = "SELECT * FROM `book` where `id`=".$this->id;
			$result = mysqli_query($this->conn, $query);
			$row = mysqli_fetch_assoc($result);

			return $row;

		}


		public function update(){
			$query = "UPDATE `book` SET `title` = '".$this->title."' WHERE `book`.`id` =".$this->id;
			$result = mysqli_query($this->conn, $query);
			if($result){
				Message::message("<div class=\"alert alert-success\">
 				 <strong>Success!</strong> Data has been updated successfully.
					</div>");
				Utility::redirect('index.php');
			}
			else{
				Message::message("<div class=\"alert alert-danger\">
 				 <strong>Oops!</strong> Data has not been updated successfully.
					</div>");
				header('Location:index.php');

				}

		}

		public function delete(){
			$query = "DELETE FROM `book` WHERE `book`.`id` =".$this->id;
			$result = mysqli_query($this->conn, $query);

			if($result){
				Message::message("<div class=\"alert alert-success\">
 				 <strong>Success!</strong> Data has been deleted successfully.
					</div>");
				Utility::redirect('index.php');
			}
			else{
				Message::message("<div class=\"alert alert-danger\">
 				 <strong>Oops!</strong> Data has not been deleted successfully.
					</div>");
				header('Location:index.php');

				}

		}

		public function trash(){

			$this->deleted_at = time();
			$query = "UPDATE `book` SET `deleted_at` = '".$this->deleted_at."' WHERE `book`.`id` =".$this->id ;
			$result = mysqli_query($this->conn, $query);

			if($result){
				Message::message("<div class=\"alert alert-success\">
 				 <strong>Success!</strong> Data has been trashed successfully.
					</div>");
				Utility::redirect('index.php');
			}
			else{
				Message::message("<div class=\"alert alert-danger\">
 				 <strong>Oops!</strong> Data has not been trased successfully.
					</div>");
				header('Location:index.php');

				}
		}

		public function trashed(){

			$_trashedBook = array();
			$query = "SELECT * FROM `book` WHERE `deleted_at` IS NOT NULL";
			$result = mysqli_query($this->conn, $query);
			while($row = mysqli_fetch_assoc($result)){
				$_trashedBook[] = $row;
			}
			return $_trashedBook;

		}

		public function recover(){

			
			$query = "UPDATE `book` SET `deleted_at` = NULL WHERE `book`.`id` =".$this->id ;
			$result = mysqli_query($this->conn, $query);

			if($result){
				Message::message("<div class=\"alert alert-success\">
 				 <strong>Success!</strong> Data has been recovered successfully.
					</div>");
				Utility::redirect('index.php');
			}
			else{
				Message::message("<div class=\"alert alert-danger\">
 				 <strong>Oops!</strong> Data has not been recovered successfully.
					</div>");
				header('Location:index.php');

				}


		}

		public function recoverMultiple($idS=array()){

			if(is_array($idS) && count($idS)>0){
				$IDs = implode(",", $idS);

			$query = "UPDATE `book` SET `deleted_at` = NULL WHERE `book`.`id` IN(".$IDs.")";
			$result = mysqli_query($this->conn, $query);
			if($result){
				Message::message("<div class=\"alert alert-success\">
 				 <strong>Success!</strong> Selected Data has been recovered successfully.
					</div>");
				Utility::redirect('index.php');
			}
			else{
				Message::message("<div class=\"alert alert-danger\">
 				 <strong>Oops!</strong>Selected Data has not been recovered successfully.
					</div>");
				header('Location:index.php');

				}


			}


		}

		public function deleteMultiple($idS=array())
		{
			if(is_array($idS) && count($idS)>0)
			{
				$IDs = implode(",", $idS);

				$query = "DELETE FROM `book` WHERE `book`.`id` IN(".$IDs.")";
				$result = mysqli_query($this->conn, $query);
				if($result){
				Message::message("<div class=\"alert alert-success\">
 				 <strong>Success!</strong> Selected Data has been deleted successfully.
					</div>");
				Utility::redirect('trashed_view.php');
				}
				else{
				Message::message("<div class=\"alert alert-danger\">
 				 <strong>Oops!</strong>Selected Data has not been recovered successfully.
					</div>");
				Utility::redirect('trashed_view.php');

				}
			
			}

		}	

}
<?php

namespace App\Items\ProfilePicture;

use App\Items\Message\Message;
use App\Items\Utility\Utility;



class ImageUploader{

	public $id="";
	public $name = "";
	public $image_name = "";
	public $conn;


	public function prepare($data=array()){
			if(array_key_exists("name", $data)){
				$this->name = $data['name'];
			}

			if(array_key_exists("image", $data)){
				$this->image_name = $data['image'];
			}

			if(array_key_exists("id", $data)){
				$this->id = $data['id'];
			}

			return $this;
		}


	public function __construct(){
			$this->conn = mysqli_connect("localhost","root","","atomicprojectcrud") or die("Database connection failed");
		}


	public function store(){

		$query = "INSERT INTO `profilepicture` (`name`, `images`) VALUES ('{$this->name}','{$this->image_name}')";
		$result = mysqli_query($this->conn, $query);

		if($result){
			Message::message("Data has been stored successfully");
			Utility::redirect('index.php');

		}	
		else{
			Message::message("Data has not been stored successfully");
			Utility::redirect('index.php');

		}
	}	

	public function index(){

			$_allInfo = array();
			$query = "SELECT * FROM `profilepicture`";
			$result = mysqli_query($this->conn, $query);
			while($row = mysqli_fetch_assoc($result)){
				$_allInfo[] = $row;
			}
			return $_allInfo;

		}

}
<?php

namespace App\Items\Hobby;
use App\Items\Message\Message;
use App\Items\Utility\Utility;

class Hobby{

	public $id="";
	public $name="";
	public $lastName="";
	public $hobby="";
	public $deleted_at;
	public $conn;

	public function prepare($data=Array()){


		if(array_key_exists("name", $data)){
			$this->name=$data['name'];
		}

		if(array_key_exists("lastname", $data)){
			$this->lastName=$data['lastname'];
		}

		if(array_key_exists("hobby", $data)){
			$this->hobby=$data['hobby'];
		}


		if(array_key_exists("id", $data)){
			$this->id=$data['id'];
		}

		return $this;

	}
	

	public function __construct(){
			$this->conn = mysqli_connect("localhost","root","","atomicprojectcrud") or die("Database connection failed");
		}


	public function store(){

			$query ="INSERT INTO `hobby` (`name`,  `lastname`, `hobbies`) VALUES ('{$this->name}','{$this->lastName}','{$this->hobby}')";
			//Utility::dd($query);
			
			$result = mysqli_query($this->conn, $query); 

			if($result){
				Message::message("<div class=\"alert alert-success\">
 				 <strong>Success!</strong> Data has been stored successfully.
					</div>");
				Utility::redirect('index.php');
			}
			else{
				Message::message("<div class=\"alert alert-danger\">
 				 <strong>Oops!</strong> Data has not been stored successfully.
					</div>");
				header('Location:index.php');

				}

			}

			public function index(){

			$_allHobby = array();
			$query = "SELECT * FROM `hobby` WHERE `deleted_at` IS NULL";
			$result = mysqli_query($this->conn, $query);
			while($row = mysqli_fetch_assoc($result)){
				$_allHobby[] = $row;
			}
			return $_allHobby;

		}

		public function view()
		{
			$query = "SELECT * FROM `hobby` where `id`=".$this->id;
			$result = mysqli_query($this->conn, $query);
			$row = mysqli_fetch_assoc($result);

			return $row;

		}

		public function update()
		{
			$query = "UPDATE `hobby` SET `name` = '".$this->name."', `lastname` = '".$this->lastName."', `hobbies` = '".$this->hobby."' WHERE `hobby`.`id` =".$this->id;
			$result = mysqli_query($this->conn, $query);

		
			if($result){
				Message::message("<div class=\"alert alert-success\">
 				 <strong>Success!</strong> Data has been updated successfully.
					</div>");
				Utility::redirect('index.php');
			}
			else{
				Message::message("<div class=\"alert alert-danger\">
 				 <strong>Oops!</strong> Data has not been updated successfully.
					</div>");
				header('Location:index.php');

				}

		}

		public function delete()
		{
			$query = "DELETE FROM `hobby` WHERE `hobby`.`id` =".$this->id;
			$result = mysqli_query($this->conn, $query);
			
			if($result){
				Message::message("<div class=\"alert alert-success\">
 				 <strong>Success!</strong> Data has been deleted successfully.
					</div>");
				Utility::redirect('index.php');
			}
			else{
				Message::message("<div class=\"alert alert-danger\">
 				 <strong>Oops!</strong> Data has not been deleted successfully.
					</div>");
				header('Location:index.php');

				}

		}

		public function trash()
		{
			$this->deleted_at = time();
			$query = "UPDATE `hobby` SET `deleted_at` = '".$this->deleted_at."' WHERE `hobby`.`id` =".$this->id;
			$result = mysqli_query($this->conn, $query);

			if($result){
				Message::message("<div class=\"alert alert-success\">
 				 <strong>Success!</strong> Data has been trashed successfully.
					</div>");
				Utility::redirect('index.php');
			}
			else{
				Message::message("<div class=\"alert alert-danger\">
 				 <strong>Oops!</strong> Data has not been trased successfully.
					</div>");
				header('Location:index.php');

				}
		
		}

		public function trashed()
		{
			$_trashedHobby = array();
			$query = "SELECT * FROM `hobby` WHERE `deleted_at` IS NOT NULL";
			$result = mysqli_query($this->conn, $query);

			while ($row = mysqli_fetch_assoc($result)) {
				$_trashedHobby[] = $row;
			}
			return $_trashedHobby;


		}

		public function recover()
		{
			$query = "UPDATE `hobby` SET `deleted_at`= NULL WHERE `hobby`.`id` =".$this->id;
			$result = mysqli_query($this->conn, $query);

			if($result){
				Message::message("<div class=\"alert alert-success\">
 				 <strong>Success!</strong> Data has been recovered successfully.
					</div>");
				Utility::redirect('trashed_view.php');
			}
			else{
				Message::message("<div class=\"alert alert-danger\">
 				 <strong>Oops!</strong> Data has not been recovered successfully.
					</div>");
				Utility::redirect('trashed_view.php');

				}	

		}
			
		public function recoverMultiple($idS=array())
		{
			if(is_array($idS) && count($idS>0))
			{
				$IDs = implode(",", $idS);

			$query = "UPDATE `hobby` SET `deleted_at`= NULL WHERE `hobby`.`id` IN (".$IDs.")";		
			$result = mysqli_query($this->conn, $query);

			if($result){
				Message::message("<div class=\"alert alert-success\">
 				 <strong>Success!</strong> Selected Data has been recovered successfully.
					</div>");
				Utility::redirect('index.php');
			}
			else{
				Message::message("<div class=\"alert alert-danger\">
 				 <strong>Oops!</strong>Selected Data has not been recovered successfully.
					</div>");
				header('Location:index.php');

				}

			}

		}

		public function deleteMultiple($idS=array())
		{
			if(is_array($idS) && count($idS)>0)
			{
				$IDs = implode(",", $idS);

				$query = "DELETE FROM `hobby` WHERE `hobby`.`id` IN(".$IDs.")";
				$result = mysqli_query($this->conn, $query);
				if($result){
				Message::message("<div class=\"alert alert-success\">
 				 <strong>Success!</strong> Selected Data has been deleted successfully.
					</div>");
				Utility::redirect('trashed_view.php');
				}
				else{
				Message::message("<div class=\"alert alert-danger\">
 				 <strong>Oops!</strong>Selected Data has not been deleted successfully.
					</div>");
				Utility::redirect('trashed_view.php');

				}
			
			}

		}			

	} 

	




